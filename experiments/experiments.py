# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import math
from matplotlib import pyplot as plt
from bokeh.plotting import figure, show
from bokeh.models import Range1d

def is_linking(name):
    return name.endswith('dll') or name.endswith('exp') or name.endswith('lib') or name.endswith('exe')

def is_autogen(name):
    return 'autogen' in name

def get_module_name(name):
    if not is_linking(name):
        return name.split('/')[1]
    return name.split('/')[-1].split('.')[0]
def get_object_name(name):
    if is_linking(name):
        return 'na'
    if 'autogen' in name:
        return 'na'
    if '/__/__/' not in name:
        #Ignore this, various special targets
        return 'na'
    return name.split('/__/__/')[1]


def processdata(df):
    df['Is_linking'] = df['Target'].apply(is_linking)
    df['Is_autogen'] = df['Target'].apply(is_autogen)
    df['Time'] = df.apply(lambda x: x[1] - x[0], axis=1)
    df['Module'] = df['Target'].apply(get_module_name)
    df['Object'] = df['Target'].apply(get_object_name)
    drop_columns = ['Start', 'Stop', 'Unknown', 'Hash']
    df.drop(drop_columns, 1, inplace=True)
    print(df)
    print(df['Object'])
    return df
    

filename = 'C:/Users/pette/Downloads/.ninja_log[1]'
df = pd.read_csv(filename, sep='\t',
                 names=['Start', 'Stop', 'Unknown', 'Target', 'Hash'],
                 skiprows=[0])
df = processdata(df)

'''
top_level = ['System', 'util']
modules = ['wellplan', 'wellplan/traj', 'wellplan/project', 'wellplan/target', 'wellplan/log', 'vol', 'intersections', 'resmod']

folders = []
times = []

for folder in top_level:
    folders.append(folder)

for folder in modules:
    folders.append('modules/%s' % folder)

for folder in folders:
    sliced_df = df[df['Target'].str.contains(folder)]
    time_ms = (sliced_df['Stop'] - sliced_df['Start']).sum()
    times.append(time_ms/1000/60)

data = {'folder' : folders,
        'times'  : times}
bla= [0.5*x for x in data['times']]
plot = figure(x_range = data['folder'], title='Test')
plot.rect(data['folder'], y=bla, height=data['times'], width=0.75)
plot.xaxis.major_label_orientation=0.6
plot.y_range = Range1d(0, math.floor(max(data['times'])* 1.1) )

show(plot)
#figure
#plt.stem(folders, times)
#plt.show()
'''
