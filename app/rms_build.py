from bokeh.models import Range1d, ColumnDataSource, TapTool, OpenURL
from bokeh.plotting import figure, show
from matplotlib import pyplot as plt
from sqlalchemy import create_engine
import math
import pandas as pd
import sqlite3 as sql
import os
import datetime

def get_dataframe(filename):
    return pd.read_csv(filename, sep='\t',
                       names=['Start', 'Stop', 'Unknown', 'Target', 'Hash'],
                       skiprows=[0])
def get_histogram(df):
    by_modules = df.groupby('Module').sum()
    modules = by_modules.index.unique().tolist()
    data = {'Module' : modules,
            'Time'  : by_modules.Time.tolist()}
    bla= [0.5*x for x in data['Time']]
    plot = figure(x_range = modules, title='Module build times')
    plot.rect(data['Module'], y=bla, height=data['Time'], width=0.75)
    plot.xaxis.major_label_orientation=0.6
    plot.y_range = Range1d(0, math.floor(max(data['Time'])* 1.1) )
    return plot

def get_lineplot(df):
    df['Upload_time'] = df['Upload_time'].apply(lambda x: x.split('.')[0])
    plot = figure(x_range = df.Upload_time.tolist(), title='Build time', tools='tap')
    source = ColumnDataSource(df)
    plot.line(x='Upload_time', y='Total_time', source=source)
    plot.xaxis.major_label_orientation=0.6
    plot.circle('Upload_time', 'Total_time', size=10, source=source)
    url = "details/@Upload_time"
    taptool = plot.select(type=TapTool)
    taptool.callback = OpenURL(url=url)

    return plot

def processdata(df):
    df['Is_linking'] = df['Target'].apply(is_linking)
    df['Is_autogen'] = df['Target'].apply(is_autogen)
    df['Time'] = df.apply(lambda x: x[1] - x[0], axis=1)
    df['Module'] = df['Target'].apply(get_module_name)
    df['Object'] = df['Target'].apply(get_object_name)
    drop_columns = ['Start', 'Stop', 'Unknown', 'Hash']
    df.drop(drop_columns, 1, inplace=True)

    return df

def post_to_db(filename, db_file):
    dataframe = get_dataframe(filename)
    upload_time = datetime.datetime.fromtimestamp(
        os.path.getmtime(filename))
    dataframe['Upload_time'] = upload_time
    total_time = dataframe.Stop[dataframe.Stop.last_valid_index()]
    dataframe = processdata(dataframe)
    cpu_time = dataframe.Time.sum()
    data = {'Upload_time' : [upload_time],
            'Total_time' : [total_time],
            'Cpu_time' : [cpu_time]}
    build_data = pd.DataFrame(data=data)
    con = sql.connect(db_file)
    dataframe.to_sql('build_data', con, if_exists='append')
    build_data.to_sql('build_times', con, if_exists='append')

def is_linking(name):
    return (name.endswith('dll') or name.endswith('exp') or
            name.endswith('lib') or name.endswith('exe') or
            name.endswith('pyd'))

def is_autogen(name):
    return 'autogen' in name

def get_module_name(name):
    if not is_linking(name):
        return name.split('/')[1]
    return name.split('/')[-1].split('.')[0]

def get_object_name(name):
    if is_linking(name):
        return 'na'
    if 'autogen' in name:
        return 'na'
    if '/__/__/' not in name:
        #Ignore this, various special targets
        return 'na'
    return name.split('/__/__/')[1]

def get_records(db_file):
    engine = create_engine('sqlite:///%s' %db_file)
    df = pd.read_sql('SELECT * FROM build_times', engine)
    return df

def get_details(db_file, timestamp):
    engine = create_engine('sqlite:///%s' %db_file)
    try:
       df = pd.read_sql('SELECT * FROM build_data WHERE Upload_time LIKE "{0}%"'.format(timestamp), engine)
    except KeyError:
       df = pd.DataFrame()
    return df
