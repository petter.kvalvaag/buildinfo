from flask import render_template, request, redirect, url_for

import os
import bokeh
import tempfile

from bokeh.embed import components

from app import app
from app import rms_build

UPLOAD_FOLDER = tempfile.gettempdir()
DB_FILE = 'db.sqlite'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['DB_FILE'] = DB_FILE

@app.route('/')
@app.route('/index')
def index():
    df = rms_build.get_records(app.config['DB_FILE'])
    plot = rms_build.get_lineplot(df)
    script, div = components(plot)
    df['Upload_time'] = df['Upload_time'].apply(lambda x: '<a href="details/{0}">{1}</a>'.format(x, x.split('.')[0]))
    table_html = df.to_html(classes='table', escape=False)
    bokeh_version = bokeh.__version__
    return render_template('index.html', title = 'Home', table_html=table_html,
                           script=script, div=div, bokeh_version=bokeh_version)

@app.route('/timing/<filename>')
def timing(filename):
    filename = 'c:/Users/pette/Downloads/.ninja_log[1]'
    df = rms_build.get_dataframe(filename)
    plot = rms_build.get_plot(df)
    script, div = components(plot)
    bokeh_version = bokeh.__version__
    return render_template("timing.html", title=filename, script=script, div=div,
                           bokeh_version=bokeh_version)

@app.route('/details/<timestamp>')
def details(timestamp):
    df = rms_build.get_details(app.config['DB_FILE'], timestamp)
    plot = rms_build.get_histogram(df)
    script, div = components(plot)
    bokeh_version = bokeh.__version__
    return render_template('details.html', title = 'Home',
                           script=script, div=div, bokeh_version=bokeh_version)



@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file:
            filename = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
            file.save(filename)
            rms_build.post_to_db(filename, app.config['DB_FILE'])
    return render_template('upload.html', title = 'Upload')
