Buildinfo
===========

This project aims to provide detailed information about C++ Ninja builds.

The concept is that after a full build, the ninja log file is posted to this server. The log is analyzed and information is stored in a local database.

To post the log files to this server, the following command is employed:

    curl -X POST -F 'file=@<ninja_log_filename>' <server_ip>:5000/upload

